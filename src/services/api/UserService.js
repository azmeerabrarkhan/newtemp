import {ApiClient} from './ApiClient';
// import URL_CONSTANTS from '../../constants/UrlConstants';
import {handleError, handleResponse} from './ResponseHandler';

export const login = async model => {
  try {
    // const response = await ApiClient().post(URL_CONSTANTS.LOGIN, model);
    const response = {
      status: 200,
      data: {
        token: 'Dummy Token',
      },
    };
    return handleResponse(response);
  } catch (error) {
    throw handleError(error);
  }
};

export default {
  login,
};
