import React from 'react';

// assets
import Assets from '../../constants/Assets.js';

// styles
import styles from './styles.js';

// components
import {View, Button, Image} from 'react-native';

class LoginFormView extends React.Component {
  render() {
    const {onlogin, loading} = this.props;
    const loginData = {username: 'userX', password: 'passX'};

    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={Assets.logo} />
        </View>
        <Button
          title="Login"
          block
          androidRippleColor="transparent"
          rounded
          style={[styles.loginButton]}
          onPress={() => onlogin(loginData)}></Button>
      </View>
    );
  }
}
export default LoginFormView;
