export function handleResponse(response) {
  if (response && response.status === 200) {
    return response.data;
  } else {
    handleError(response);
  }
}

export function handleError(error) {
  let errorMessage = '';

  if (typeof error === 'string') {
    errorMessage = error;
  } else if (error?.response?.data) {
    errorMessage =
      error.response.data.errors?.length > 0
        ? error.response.data.errors.join('. ')
        : error.response.data.message || '';
  } else if (error?.toJSON?.()?.message) {
    errorMessage = error.toJSON().message;
  } else {
    errorMessage = 'Network error';
  }

  throw errorMessage;
}
