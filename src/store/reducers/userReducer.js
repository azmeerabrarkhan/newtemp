import {USER_ACTIONS} from '../../constants/ActionConstants';

const initialState = {
  loading: false,
  token: null,
};

export function userReducer(state = initialState, action) {
  switch (action.type) {
    case USER_ACTIONS.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        token: null,
      };
    case USER_ACTIONS.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        token: action.payload,
      };
    case USER_ACTIONS.LOGIN_FAILURE:
    case USER_ACTIONS.LOGOUT_SUCCESS:
      return {
        ...state,
        loading: false,
        token: null,
      };
    default:
      return state;
  }
}
