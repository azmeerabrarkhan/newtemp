import {StyleSheet, Platform} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 60,
    // justifyContent: 'center',
    marginTop: Platform.OS === 'ios' ? 44 : 28,
  },
  loginButton: {
    marginTop: 8,
    height: Platform.OS === 'ios' ? 50 : 46,
    marginHorizontal: Platform.OS === 'ios' ? 35 : 25,
    // backgroundColor: customColor.defaultGreen,
  },
  logo: {
    alignItems: 'center',
    marginTop: Platform.OS === 'ios' ? 28 : 18,
  },
});
