import React from 'react';

// components
import {View, Button} from 'react-native';

// styles
import styles from './styles.js';

class DashboardComponent extends React.Component {
  render() {
    const {onlogout} = this.props;

    return (
      <View style={styles.container}>
        <Button
          title="Logout"
          block
          androidRippleColor="transparent"
          rounded
          style={[styles.loginButton]}
          onPress={onlogout}></Button>
      </View>
    );
  }
}
export default DashboardComponent;
