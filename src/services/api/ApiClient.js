import axios from 'axios';

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export class AccessToken {
  static token;
  static source = source;
}

export const ApiClient = () => {
  const baseUrl = Environment.config().baseURL;
  const token = AccessToken.token;
  const defaultOptions = {
    cancelToken: AccessToken.source?.token,
    headers: {
      Authorization: token ? `Bearer ${token}` : '',
      'Content-Type': 'application/json',
    },
  };

  return {
    get: (url, options = {}) =>
      axios.get(`${baseUrl}${url}`, { ...defaultOptions, ...options }),
    post: (url, data, options = {}) =>
      axios.post(`${baseUrl}${url}`, data, { ...defaultOptions, ...options }),
    put: (url, data, options = {}) =>
      axios.put(`${baseUrl}${url}`, data, { ...defaultOptions, ...options }),
    delete: (url, options = {}) =>
      axios.delete(`${baseUrl}${url}`, { ...defaultOptions, ...options }),
  };
};

export const BuildMode = {
  LOCAL: 'LOCAL',
  DEV: 'DEV',
  QA: 'QA',
  STAGING: 'STAGING',
  PROD: 'PROD',
  DEMO: 'DEMO',
};

export const Environment = {
  // buildMode: BuildMode.LOCAL,
  buildMode: BuildMode.DEV,
  // buildMode: BuildMode.QA,
  // buildMode: BuildMode.DEMO,
  // buildMode: BuildMode.STAGING,

  /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */

  configurations: {
    [BuildMode.LOCAL]: {
      baseURL: '',
    },
    [BuildMode.DEV]: {
      baseURL: 'http://demo1.folio3.com:8983/api/v1',
    },
    [BuildMode.QA]: {
      baseURL: 'https://cfpvapi.azurewebsites.net/api/v1',
    },
    [BuildMode.STAGING]: {
      baseURL: 'https://apps.cactusfeeders.com/cfapitest/api/v1',
    },
    [BuildMode.PROD]: {
      baseURL: '',
    },
    [BuildMode.DEMO]: {
      baseURL: '',
    },
  },

  config(buildMode) {
    const mode = buildMode || this.buildMode;
    return this.configurations[mode];
  },
};
