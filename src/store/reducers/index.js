import {userReducer} from './userReducer';
import {appReducer} from './appReducer';

export default {
  app: appReducer,
  user: userReducer,
};
