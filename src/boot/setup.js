import React from 'react';

// components
import {Provider} from 'react-redux';

// store
import store from '../store';

// screens
import App from './App';

class Setup extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

export default Setup;
