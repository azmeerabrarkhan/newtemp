import {
  getAccessToken,
  setAccessToken,
  deleteLoginData,
} from '../../services/StorageService';
//   import { AccessToken } from '../../services/api/ApiClient';
import {actionWrapper} from '..';
import UserService from '../../services/api/UserService';
import {USER_ACTIONS} from '../../constants/ActionConstants';
// import axios from 'axios';
// import PVMobileDB from '../../database';

async function deleteLocalData() {
  // const CancelToken = axios.CancelToken;
  // const source = CancelToken.source();

  // AccessToken.token = null;
  // AccessToken.source?.cancel?.();
  // AccessToken.source = source;

  deleteLoginData();

  // PVMobileDB.getConnection().then(db => {
  //   db.write(() => {
  //     db.deleteAll();
  //   });
  // });
}

export const login = model => {
  return async dispatch => {
    try {
      dispatch(actionWrapper(USER_ACTIONS.LOGIN_REQUEST));
      const loginResponse = await UserService.login(model);
      const token = loginResponse?.token || null;
      // AccessToken.token = token;
      dispatch(actionWrapper(USER_ACTIONS.LOGIN_SUCCESS, token));
      await setAccessToken(token);
    } catch (error) {
      dispatch(actionWrapper(USER_ACTIONS.LOGIN_FAILURE, error));
      // AccessToken.token = '';
      throw error;
    }
  };
};

export const logout = () => {
  return async dispatch => {
    try {
      dispatch(actionWrapper(USER_ACTIONS.LOGOUT_REQUEST));
      await deleteLocalData();
      dispatch(actionWrapper(USER_ACTIONS.LOGOUT_SUCCESS));
    } catch (error) {
      console.log(error);
      dispatch(actionWrapper(USER_ACTIONS.LOGOUT_FAILURE, error));
      throw error;
    }
  };
};

export const userAutoLoggedIn = () => {
  return async dispatch => {
    const token = await getAccessToken();
    // AccessToken.token = token;
    dispatch(actionWrapper(USER_ACTIONS.LOGIN_SUCCESS, token));
  };
};
