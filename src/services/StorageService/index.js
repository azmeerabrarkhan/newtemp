import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOCAL_STORAGE_KEYS} from '../../constants/GenericConstants';
// import { isValid } from 'date-fns';

export const setAccessToken = async accessToken => {
  await AsyncStorage.setItem(
    LOCAL_STORAGE_KEYS.ACCESS_TOKEN,
    accessToken || '',
  );
};

export const getAccessToken = async () => {
  try {
    return await AsyncStorage.getItem(LOCAL_STORAGE_KEYS.ACCESS_TOKEN);
  } catch (error) {
    return null;
  }
};

// export const setUserData = async (userData) => {
//   await AsyncStorage.setItem(
//     LOCAL_STORAGE_KEYS.USER_DATA,
//     JSON.stringify(userData) || '',
//   );
// };

// export const getUserData = async () => {
//   try {
//     return JSON.parse(
//       await AsyncStorage.getItem(LOCAL_STORAGE_KEYS.ACCESS_TOKEN),
//     );
//   } catch (error) {
//     return null;
//   }
// };

// export const setLastSync = async (key, value) => {
//   if (value) {
//     value = String(value);
//     await AsyncStorage.setItem(key, value);
//   }
// };

// export const getLastSync = async (key) => {
//   try {
//     const timestamp = (await AsyncStorage.getItem(key)) || '';
//     const date = new Date(timestamp);
//     return timestamp && isValid(date) ? timestamp : null;
//   } catch (error) {
//     return null;
//   }
// };

export const deleteLoginData = async () => {
  await AsyncStorage.clear();
};
