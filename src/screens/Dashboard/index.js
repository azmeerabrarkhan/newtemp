import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

//components
import {View} from 'react-native';
import DashboardComponent from '../../components/Dashboard';

// actions
import {logout} from '../../store/actions/userAction';

class DashboardScreen extends React.Component {
  handleLogout = () => {
    this.props.logout();
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#e3e3e3'}}>
        <DashboardComponent onlogout={this.handleLogout} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  const {isConnected} = state.app;
  return {isConnected};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({logout}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);
