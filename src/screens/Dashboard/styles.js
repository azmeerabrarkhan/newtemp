import customColor from '../../themes/variables/customColor';
import customFont from '../../themes/variables/customFont';

export default {
  container: {
    flex: 1,
    backgroundColor: customColor.white,
  },
  taskDueContainer: {
    margin: 15,
  },
  title: {
    fontFamily: "Lato",
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    color: "#000000",
    margin: 16,
    marginBottom: 6,
  },
  mainHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
    fontWeight: 'bold',
    //  fontFamily: 'Lato',
    alignItems: 'center',
  },
  mainTitle: {
    // paddingLeft: 10,
    fontSize: customFont.cardTitleSize,
    fontFamily: 'Lato',
    paddingTop: 2,
    fontWeight: 'bold',
  },
  filterControl: {
    backgroundColor: customColor.defaultGreen,
    borderRadius: 20,
    width: 36,
    height: 36,
    // textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    //alignSelf: 'flex-end',
    marginRight: 5,
  },
  filterIcon: {
    fontSize: 16,
    fontFamily: 'Lato',
    color: 'white',
  },
  statusSegment: {
    tabsContainerStyle: {
      borderColor: customColor.lightGrey,
      backgroundColor: customColor.white,
      borderWidth: 1,
      borderRadius: 25,
      marginBottom: 14,
    },
    tabStyle: {
      borderColor: 'transparent',
      height: 40,
      justifyContent: 'center',
    },
    firstTabStyle: {},
    lastTabStyle: {},
    tabTextStyle: {
      fontSize: customFont.cardFieldSize,
      fontFamily: 'Lato',
      color: 'rgb(79, 81, 77)',
      letterSpacing: 0,
      textAlign: 'center',
    },
    activeTabStyle: {
      backgroundColor: customColor.defaultGreen,
      borderRadius: 25,
    },
    activeTabTextStyle: {
      fontSize: customFont.cardFieldSize,
      fontFamily: 'Lato',
      color: customColor.white,
      textAlign: 'center',
    },
    tabBadgeContainerStyle: {},
    activeTabBadgeContainerStyle: {},
    tabBadgeStyle: {},
    activeTabBadgeStyle: {
      //custom styles
    },
  },
  taskDue: {
    marginTop: 12,
  },
  syncIndicator: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
  },
};
