// modules
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// components
import {StatusBar, View} from 'react-native';
import LoginFormView from '../../components/Login';

// models
import {LoginModel} from '../../models/user';

// actions
import {login} from '../../store/actions/userAction';

class LoginScreen extends React.Component {
  handleLogin = async values => {
    if (!this.props.isConnected) {
      console.log('Limited or No Connectivity');
    } else {
      try {
        const {username, password} = values;
        if (username && password) {
          const request = LoginModel(username, password);
          await this.props.login(request);
        }
      } catch (error) {
        console.log('Login failed. Please try again.');
        console.log(error);
      }
    }
  };

  render() {
    const {loading} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: '#e3e3e3'}}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <LoginFormView onlogin={this.handleLogin} loading={loading} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  const {token, loading} = state.user;
  const {isConnected} = state.app;
  return {token, loading, isConnected};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({login}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
